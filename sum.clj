; Sum It All Up http://www.4clojure.com/problem/24

(fn ! [x]
  (if (= (count x) 0)
    0
    (+ (first x) (! (rest x)))
    )
  )

(def pi 22/7)
(+ 1 pi)
