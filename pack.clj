; http://www.4clojure.com/problem/31


(def (fn pack [seq]
       (reverse
        (reduce (fn rad [[frst & rst] nxt]
                  (if (= (first frst)  nxt)
                    (conj rst (conj frst nxt))
                    (conj (conj rst frst) (list nxt)))) 
                (list (list (first seq)))
                (rest seq)))))

(pprint (pack '(1 1 2 2 3 4 4 5 5)))
