(defprotocol FIXO
  (fixo-push [fixo value])
  (fixo-pop [fixo])
  (fixo-peek [fixo]))

(defrecord TreeNode [val l r])

(defn xconj [t v]
  (cond
   (nil? t)       (TreeNode. v nil nil)
   (< v (:val t)) (TreeNode. (:val t) (xconj (:l t) v) (:r t))
   :else          (TreeNode. (:val t) (:l t) (xconj (:r t) v))))


(defn xseq [t]
   (when t
    (concat (xseq (:l t)) [(:val t)] (xseq (:r t)))))

(def sample-tree (reduce xconj nil [3 5 2 4 6]))


(extend-type TreeNode
  FIXO
  (fixo-push [node value]
    (xconj node value)))


(extend-type clojure.lang.IPersistentVector
  FIXO
  (fixo-push [vector value]
    (conj vector value))
  (fixo-peek [vector]
    (peek vector))
  (fixo-pop [vector]
    (pop vector)))

(defn fixed-fixo
  ([limit] (fixed-fixo limit []))
  ([limit vector]
    (reify FIXO
      (fixo-push [this value]
        (if (< (count vector) limit)
          (fixed-fixo limit (conj vector value))
          this))
      (fixo-peek [_]
        (peek vector))
      (fixo-pop [_]
        (pop vector)))))


(prn (fixo-peek (fixo-push (fixed-fixo 3 [1 2 3]) 5)))
; => 3
(prn (fixo-peek (fixo-push (fixed-fixo 4 [1 2 3]) 5)))
; => 5
