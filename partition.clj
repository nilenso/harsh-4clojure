; http://www.4clojure.com/problem/54
; Write a function which returns a sequence of lists of x items each. Lists of less than x items should not be returned.

(def part (fn _part [n _seq]
            (if (< (count _seq) n)
              (list)
              (let [[p1 p2] (split-at n _seq)]
                (conj (_part n p2) p1)))))


(assert (= (part 3 (range 9)) (partition 3 (range 9))))
(assert (= (part 2 (range 8)) (partition 2 (range 8))))
