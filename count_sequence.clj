;Count a Sequence http://www.4clojure.com/problem/22
(fn ! [x] 
  (if (= () x)
    0
    (+ (! (rest x)) 1)
  )
)
