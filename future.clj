; Exercise 2, Chapter 6, Clojure from Ground Up

; Ques: We can do the computation in a new thread directly, using (.start (Thread. (fn [] (sum 0 1e7))))–but this simply runs the (sum) function and discards the results. Use a promise to hand the result back out of the thread. Use this technique to write your own version of the future macro.

(defn sum [start end] (reduce + (range start end)))

(def ox (promise))

(.start (Thread. (deliver ox (sum 0 1e7)))) 

(defmacro fut [args]
  (prn args)
  (def x (promise))
  (.start (Thread. (deliver x (eval args))))
  x)

(def y (fut (sum 0 1e7)))
(prn (deref y))

;; Exercise 3

;; Ques:

;; (def s (+ (future (sum 0 (/ 1e7 2))) (future (sum (/ 1e7 2)) 1e7)))

(def p1 (future (sum 0 (/ 1e7 2))))
(def p2 (future (sum (/ 1e7 2) 1e7)))
(time (def s (+ (deref p1) (deref p2))))

;; Time elapsed 305 micro seconds
;; Evaluating sum in 1 part it takes 664 micro seconds

