; Write a function which separates the items of a sequence by an arbitrary value.
; http://www.4clojure.com/problem/40

(def ipos (fn [x seq]
            (butlast (mapcat list seq (repeat x)))))

(pprint (ipos 0 [1 2 3]))
(pprint (ipos :z [:a :b :c :d]))
