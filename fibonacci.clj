; Write a function to return first X fibonacci numbers http://www.4clojure.com/problem/26

(defn fibo [seq]
  (concat seq `(~(+ (last seq) (last (butlast seq))))))

(defn nfibo [n]
  (nth (iterate fibo '(1 1)) (- n 2)))

(println (nfibo 4))

;; -- Inline function

((fn fibo [n]
    (if (= n 2)
      '(1 1)
      (let [seq (fibo (dec n))]
        (concat seq `(~(+ (last seq) (last (butlast seq)))))))) 3)
