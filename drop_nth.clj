; Write a function which drops every Nth item from a sequence.
; http://www.4clojure.com/problem/41

(def dropn (fn [seq n]
             (map last (filter #(not= (mod (+ 1 (first %)) n) 0)
                                 (map-indexed (fn [& x] x) seq)))))

(pprint (dropn [1 2 3 4 5 6 7 8] 3))

; --- Alternate --

(defn dropn [seq n]
  (flatten (partition-all (dec n) n seq)))

(pprint (dropn [1 2 3 4 5 6 7 8] 3))
(pprint (dropn [1 2 3 4 5 6 7 8] 4))
