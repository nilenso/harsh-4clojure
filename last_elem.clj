;Last Element http://www.4clojure.com/problem/19
(fn ! [x] 
  (if (= (count x) 1)
    (first x)
    (! (rest x))
  )
)
