; Exercise 4, Chapter 5, Clojure from Ground Up
; Write a macro log which uses a var, logging-enabled, to determine whether or not to print an expression to the console at compile time. If logging-enabled is false, (log :hi) should macroexpand to nil. If logging-enabled is true, (log :hi) should macroexpand to (prn :hi)

; Ques: Why would you want to do this check during compilation, instead of when running the program? What might you lose?
; Ans: Compile time macro expanstions are faster though they might be harder to debug

(defmacro log [logging-enabled]
  (if logging-enabled
    '(prn :hi)
    'nil))

(pprint (macroexpand '(log true)))
(pprint (macroexpand '(log false)))
