; www.4clojure.com/problem/63
; Given a function f and a sequence s, write a function which returns a map. The keys should be the values of f applied to each item in s. The value at each key should be a vector of corresponding items in the order they appear in s.

(def my-group-by (fn [f coll] (reduce 
                               (fn [m v]
                                 (let [k (f v)]
                                   (assoc m k (conj (get m k []) v))))
                               {} coll)))

(assert (= (my-group-by #(> % 5) [1 3 6 8]) {false [1 3], true [6 8]}))
(assert (= (my-group-by #(apply / %) [[1 2] [2 4] [4 6] [3 6]]) {1/2 [[1 2] [2 4] [3 6]], 2/3 [[4 6]]}))
(assert (= (my-group-by count [[1] [1 2] [3] [1 2 3] [2 3]])
           {1 [[1] [3]], 2 [[1 2] [2 3]], 3 [[1 2 3]]}))
