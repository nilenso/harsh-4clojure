(require '[clojure.walk :as walk])
(require '[clojure.core.logic.pldb :as pldb])
(require '[clojure.core.logic :as logic])

(defn lvar?
  "Determines if a value represents a logic variable"
  [x]
  (boolean
   (when (symbol? x)
     (re-matches #"^\?.*" (name x)))))

(defn satisfy
  [l r knowledge]
  (let [L (get knowledge l l)
        R (get knowledge r r)]
    (cond
     (not knowledge)        nil
     (= L R)          knowledge
     (lvar? L) (assoc knowledge L R)
     (lvar? R) (assoc knowledge R L)
     (every? seq? [L R])
     (satisfy (rest L)
              (rest R)
              (satisfy (first L)
                       (first R)
                       knowledge))
     :default nil)))

(defn subst [term binds]
  (walk/prewalk
   (fn [expr]
     (if (lvar? expr)
       (or (binds expr) expr)
       expr))
   term))

(defn meld [term1 term2]
  (->> {}
       (satisfy term1 term2)
       (subst term1)))

(def l '(1 ?x 4))
(def r '(1 2 4))


(pldb/db-rel orbits orbital body)
(pldb/db-fact orbits :mercury :sun)
(pldb/db-fact orbits :venus :sun)
(pldb/db-fact orbits :earth :sun)
(pldb/db-fact orbits :mars :sun)
(pldb/db-fact orbits :jupiter :sun)
(pldb/db-fact orbits :saturn  :sun)
(pldb/db-fact orbits :uranus  :sun)
(pldb/db-fact orbits :neptune :sun)

(logic/run* [q]
  (logic/fresh [orbital body]
    (orbits orbital body)
    (logic/== q orbital)))
