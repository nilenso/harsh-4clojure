; http://www.4clojure.com/problem/33
; Write a function which replicates each element of a sequence a variable number of times.

(def rep (fn [seq n]
           (apply interleave (repeat n seq))))

(pprint (rep 
'(1 2 3) 4))
