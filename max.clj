(def max_ (fn [& args] (reduce (fn [x y] (if (> x y) x y))
                              Integer/MIN_VALUE args)))

(pprint (max_ 1 2 3 -1 4 5))
