; Duplicate the items of a sequence http://www.4clojure.com/problem/32

(def dup_ (fn dup [seq] 
           (if (empty? seq)
             seq
             (concat (list (first seq) (first seq)) (dup (rest seq))))))

(pprint (dup_ '(1 2 3)))
