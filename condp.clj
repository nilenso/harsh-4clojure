(defn multipleof? [n]
  (condp (fn [x y] (= (mod x y) 0)) n
    2 :two
    3 :three
    5 :five
    7 :seven
      :other))
