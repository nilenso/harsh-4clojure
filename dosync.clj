; Exercise 5, Chapter 6, Clojure from Ground Up

(def work (ref (apply list (range 1e5))))

(def sum (ref 0))

(defn dowork []
  (future
    (while (not (empty? @work))
      (dosync
       (alter sum + (first @work))
       (ref-set work (rest @work)))))
  (future
    (while (not (empty? @work))
      (dosync
       (alter sum + (first @work))
       (ref-set work (rest @work))))))

; How do I measure time of run? dowork returns instantaneously
