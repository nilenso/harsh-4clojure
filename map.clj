; http://www.4clojure.com/problem/61
; Write a function which takes a vector of keys and a vector of values and constructs a map from them.

(fn [ks vs] (apply hash-map (interleave ks vs)))
