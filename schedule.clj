; Exercise from "Clojure from the ground up"
; Using the control flow constructs we’ve learned, write a schedule function which, given an hour of the day, returns what you’ll be doing at that time. (schedule 18), for me, returns :dinner

(defn schedule [hour]
  (condp >= hour
    8 :sleep
    9 :bath
    10 :breakfast
    18 :office
    20 :dinner
    22 :reading
    :sleep))
