; Get the first 100 prime number

; Very Basic Bad version
(defn prime? [x]
  (if (= (count 
          (filter #(= (mod x %) 0) (range 1 (inc x)))) 
         2)
    true
    false))

(take 100 (filter prime? (iterate inc 0)))

; Define primes as a lazy sequence

(defn genprimes [n candi]
  (let [new-candi (filter #(not= (mod % n) 0) candi)]
    (lazy-seq (cons n (genprimes (first new-candi) new-candi)))))y

(def primes (genprimes 2 (iterate inc 3)))



; Mutually recursive version

(defn isprime? [n]
  (empty? (filter #(= (mod n %) 0) (primes (int (Math/sqrt n))))))

(defn primes [n]
  (if (< n 2)
    []
    (if (= n 2)
      [2]
      (if (isprime? n)
        (conj (primes (dec n)) n)
        (recur (dec n))))))

(time (last (trampoline primes 10000)))
