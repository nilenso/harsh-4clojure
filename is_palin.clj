; Check Palindrome http://www.4clojure.com/problem/27
(def palin (fn [x]
             (if (= (type x) (type "car"))
               (= x (apply str (reverse x)))
                 (= x (reverse x)))))

(true? (palin? "racecar"))
