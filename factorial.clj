; Write a function to calculate factorial
; http://www.4clojure.com/problem/42

(def fun #(reduce * (range 1 (inc %))))

(pprint (fun 1))
(pprint (fun 5))
