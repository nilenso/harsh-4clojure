; http://www.4clojure.com/problem/62
; Given a side-effect free function f and an initial value x write a function which returns an infinite lazy sequence of x, (f x), (f (f x)), (f (f (f x))), etc.

(def my-iterate (fn _iterate [f x] (lazy-seq (cons x (map f (_iterate f x))))))

(assert (= (take 5 (my-iterate #(* 2 %) 1)) [1 2 4 8 16]))
(assert (= (take 100 (my-iterate inc 0)) (take 100 (range))))
