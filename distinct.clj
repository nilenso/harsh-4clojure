; http://www.4clojure.com/problem/56
; Write a function which removes the duplicates from a sequence. Order of the items must be maintained.

; Not very efficient but works!
; Remember contains? doesn't work as expected for numerically indexed collections
(def fun (fn [coll] (first (reduce (fn [[res seenset] v] (if (contains? seenset v) [res seenset] [(conj res v) (conj seenset v)])) [[] #{}] coll))))

(assert (= (fun [1 2 1 3 1 2 4]) [1 2 3 4]))
(assert (= (fun [:a :a :b :b :c :c]) [:a :b :c]))
