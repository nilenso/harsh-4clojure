; Exercise 3; Chapter 5, Clojure from Ground Up

; Write a macro id which takes a function and a list of args: (id f a b c), and returns an expression which calls that function with the given args: (f a b c)

(defmacro id [f & args]
  ;(pprint [f args])
  `(apply ~f '~args))

(macroexpand '(id max 1 2 3 4))
 
(pprint (id max 1 2 3 4))
