; http://www.4clojure.com/problem/58
; Write a function which allows you to create function compositions. The parameter list should take a variable number of functions, and create a function that applies them from right-to-left.

(def my-comp (fn _comp 
               ([f] f)
               ([f & fs]
                (fn [& args]
                  (f (apply (apply _comp fs) args))))))

(assert (= ((my-comp rest reverse) [1 2 3 4]) [3 2 1]))
