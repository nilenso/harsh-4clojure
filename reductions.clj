; http://www.4clojure.com/problem/60
; Write a function which behaves like reduce, but returns each intermediate value of the reduction. Your function must accept either two or three arguments, and the return sequence must be lazy.

(def my-reductions (fn red 
                     ([f init lazycoll]
                      (if (empty? lazycoll)
                        [init]
                        (let [val (f init (first lazycoll))]
                          (lazy-seq (cons init (red f val (rest lazycoll)))))))
                     ([f lazycoll]
                      (red f (first lazycoll) (rest lazycoll)))))

(assert (= (take 5 (my-reductions + (range))) [0 1 3 6 10]))
(assert (= (my-reductions conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]]))
