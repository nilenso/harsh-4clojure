; Write a function which takes two sequences and returns the first item from each, then the second item from each, then the third, etc.
; http://www.4clojure.com/problem/39

(def _interleave (fn [& seqs]
                   ((fn aux [rv seqs]
                       (if (some nil? (map first seqs))
                         rv
                         (aux (concat rv (map first seqs)) (map rest seqs))))
                    '() seqs)
                   ))

(pprint (_interleave [1 2 3] [4 5 6]))
(pprint (_interleave [1 2 3] [:a :b :c] [4 5 6]))
(pprint (_interleave [5] [:a :b :c]))
