; Exercise 5, Chapter 5, Clojure from ground up

; Using the rationalize function, write a macro exact which rewrites any use of +, -, *, or / to force the use of ratios instead of floating-point numbers. (* 2452.45 100) returns 245244.99999999997, but (exact (* 2452.45 100)) should return 245245N

(defmacro exact [expr]
  (map (fn rep [x] (if (list? x)
                 (map rep x)
                 (if (float? x)
                               (rationalize x)
                               x)
                 ))
 expr))

(pprint (macroexpand '(exact (* 2452.45 100))))
(pprint (exact (* 2452.45 100)))
(pprint (exact (+ 10 (* 2452.45 100))))
