; http://www.4clojure.com/problem/34
; Write a function which creates a list of all integers in a given range.
(def range_ (fn [x y] (take (- y x) (iterate inc x))))

(pprint (range_ 1 4))
(pprint (range_ -2 2))
