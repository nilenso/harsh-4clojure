;Nth Element http://www.4clojure.com/problem/21
(fn ! [x] 
  (if (= (count x) 2)
    (first x)
    (! (rest x))
  )
)
