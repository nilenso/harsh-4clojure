; Execise 1, Chapter 6, Clojure from ground up
;
; Finding the sum of the first 10000000 numbers takes about 1 second on my machine:
; user=> (defn sum [start end] (reduce + (range start end)))
; user=> (time (sum 0 1e7))
; "Elapsed time: 1001.295323 msecs"
; 49999995000000

; Ques: Use delay to compute this sum lazily; show that it takes no time to return the delay, but roughly 1 second to deref.


(def sum (delay (reduce + (range 0 1e7))))

;; Output
;; user> (time sum)
;; "Elapsed time: 0.027595 msecs"
;; #object[clojure.lang.Delay 0x773d0506 {:status :pending, :val nil}]
;; user> (time (deref sum))
;; "Elapsed time: 877.623353 msecs"
;; 49999995000000
