; Lazy Quick Sort from Joy of Clojure
; Copied this here to understand how it work
; This is damn clever

(defn sort-parts [work]
  (lazy-seq
   (println "work" work)
   (loop [[part & parts] work]
     (println "part" part)
     (println "parts" parts)
     (if-let [[pivot & xs] (seq part)]
       (let [smaller? #(< % pivot)]
         (recur (list*
                 (filter smaller? xs)
                 pivot
                 (remove smaller? xs)
                 parts)))
       (when-let [[x & parts] parts]
         (cons x (sort-parts parts)))))))

(defn qsort [xs]
  (sort-parts (list xs)))


(qsort '(2 1 3 4))
