; Exercise 4, Chapter 6, Clojure from Ground Up
; Instead of using reduce, store the sum in an atom and use two futures to add each number from the lower and upper range to that atom. Wait for both futures to complete using deref, then check that the atom contains the right number. Is this technique faster or slower than reduce? Why do you think that might be?


(defn sumatm [start end]
  (def rv (atom 0))
  (doseq [x (range start (/ (+ start end) 2))]
    (future (swap! rv + x)))
  (doseq [y (range (/ (+ start end) 2) end)]
    (future (swap! rv + y)))
  ;(prn @rv)
  @rv)

;(prn (time (sumatm 0 1e7)))

; This is way slower than the reduce form took
; 23602 milli seconds. This is probably because there is some overhead in creating another thread and then swaping it in atom.


(defn sumatm2 [start end]
  (def rv (atom 0))
  (future (doseq [x (range start (/ (+ start end) 2))]
           (prn @rv)
            (swap! rv + x)))
  (future (doseq [y (range (/ (+ start end) 2) end)]
            (swap! rv + y)))
  ;(prn @rv)
  @rv)

; (prn (time (sumatm2 0 10)))
; (prn (sumatm2 0 10))

; This doesn't produce the right result, it looks the reason is that the function is returing the value of rv
; before the future thread can complete

(defn sumatm3 [start end]
  (def rv (atom 0))
  (def x (future (doseq [x (range start (/ (+ start end) 2))]
                   ;(prn @rv)
                   (swap! rv + x))))
  (def y (future (doseq [y (range (/ (+ start end) 2) end)]
                   (swap! rv + y))))
  ;(prn @rv)
  (deref x)
  (deref y)
  @rv)

(prn (time (sumatm3 0 10)))
(prn (sumatm3 0 10))

; Okay this for 0 - 1e7 this takes 890 milli seconds which is way better than the first version, but it is still slow than the reduce version which takes 620 milli seconds. I guess the overhead for swap is still quite a lot
