; Flatten a list http://www.4clojure.com/problem/28
(def (fn flat [seq]
       (if (not-any? coll? seq)
         seq
         (apply concat (map 
                        (fn [x]
                          (if-not (coll? x)
                            (list x)
                            (flat x)
                            ))
                        seq)))))
(pprint (flat '((((1))))))
(pprint (flat '(1 2 3 4)))
(pprint (flat '((1 2 3) 4)))
(pprint (flat '(1)))
(pprint (flat '(1 (2))))
