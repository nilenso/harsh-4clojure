(def rotate (fn [n seq]
              (let [N (count seq)
                    r (mod (+ N n) N)]
                (flatten (reverse (split-at r seq))))))

(pprint (rotate 2 [1 2 3 4 5]))
(pprint (rotate -2 [1 2 3 4 5]))
(pprint (rotate 6 [1 2 3 4 5]))
