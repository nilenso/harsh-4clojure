; Compress a sequence to remove consequetive duplicate elements http://www.4clojure.com/problem/30

(defn compress 
  ([seq]
   (compress `(~(last seq)) (butlast seq)))
  ([res seq]
   ;(pprint [res seq])
   (if-let [lastelem (last seq)]
     (if (= lastelem (first res))
       (compress res (butlast seq))
       (compress (cons lastelem res) (butlast seq)))
     res)))
(pprint (compress [1 1 2 3 3 2 2 3]))
