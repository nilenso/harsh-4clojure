;Penultimate Element http://www.4clojure.com/problem/20
(fn ! [x]
  (if (= (count x) 2)
    (first x)
    (! (rest x))
    )
)
