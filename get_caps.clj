; Take a string and output another string removing all non capital letters http://www.4clojure.com/problem/29
(import (java.lang Character))
(def getcaps (fn [seq]
               (apply str (filter (fn [x] (Character/isUpperCase x)) seq))))

(getcaps "HeLlO, WoRlD!")
(getcaps "nothing")
