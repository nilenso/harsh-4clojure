(defn make-set ([x] #{x})
  ([x & y] (clojure.set/union #{x} (make-set y))))
