; Write a function which returns a map containing the number of occurences of each distinct item in a sequence.
; http://www.4clojure.com/problem/55

(def freq (fn [coll]
            (reduce (fn [m v] (assoc m v (inc (get m v 0)))) {} coll)))

(assert (= (freq [1 1 2 3 2 1 1]) {1 4, 2 2, 3 1}))
(assert (= (freq [:b :a :b :a :b]) {:a 2, :b 3}))
