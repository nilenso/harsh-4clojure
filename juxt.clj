; Take a set of functions and return a new function that takes a variable number of arguments and returns a sequence containing the result of applying each function left-to-right to the argument list.
; http://www.4clojure.com/problem/59

(def my-juxt (fn _juxt [& fs]
               (fn [& args]
                 (mapv #(apply % args) fs))))

(assert (= ((my-juxt + max min) 2 3 5 1 6 4) [21 6 1]))
