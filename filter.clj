; Write your own version of filter

(defn nfilter [f seq]
  (if (empty? seq)
    '()
    (if (f (first seq))
      (conj (nfilter f (rest seq)) (first seq))
      (nfilter f (rest seq)))))
