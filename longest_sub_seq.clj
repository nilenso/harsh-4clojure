; Given a vector of integers, find the longest consecutive sub-sequence of increasing numbers. If two sub-sequences have the same length, use the one that occurs first. An increasing sub-sequence must have a length of 2 or greater to qualify.

;(def fun )

(def lsubseq (fn [seq]
               ; Both rv and curseq are vectors
               (let [fun (fn [[rv curseq] elem]
                                      (if (> elem (last curseq))
                                        [rv (conj curseq elem)]
                                        (if (> (count curseq) (count rv))
                                          [curseq [elem]]
                                          [rv [elem]])))
                      parts (reduce fun [[(first seq)] [(first seq)]] (rest seq))
                      p1 (first parts)
                      p2 (second parts)]
                 (if (= (count (max-key count p2 p1)) 1)
                   []
                   (max-key count p2 p1)))))

(assert (= (lsubseq [1 0 1 2 3 0 4 5]) [0 1 2 3]))
(assert (= (lsubseq [5 6 1 3 2 7]) [5 6]))
(assert (= (lsubseq [2 3 3 4 5]) [3 4 5]))
(assert (= (lsubseq [7 6 5 4]) []))
