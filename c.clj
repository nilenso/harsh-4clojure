; Find the number of 'c's in "abracadabra"

(count (filter #(= \c %) "abracadabra"))
