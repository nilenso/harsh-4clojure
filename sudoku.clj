; Sudoku solver from Joy of Clojure
;(use '(clojure.set set))
;(use '[clojure.set :only (index)])
(def b1 '[- - 3 - 2 - 6 - - 
          9 - - 3 - 5 - - 1
          - - 1 8 - 6 4 - - 
          - - 8 1 - 2 9 - - 
          7 - - - - - - - 8
          - - 6 7 - 8 2 - - 
          - - 2 6 - 9 5 - - 
          8 - - 2 - 3 - - 9
          - - 5 - 1 - 3 - - ])

(defn prep [board]
  (map #(partition 3 %)
       (partition 9 board)))

(defn print-board [board]
  (let [row-sep (apply str (repeat 37 "-"))]
    (println row-sep)
    (dotimes [row (count board)]
      (print "| ")
      (doseq [subrow (nth board row)]
        (doseq [cell (butlast subrow)]
          (print (str cell "   ")))
        (print (str (last subrow) " | ")))
      (println)
      (when (zero? (mod (inc row) 3))
        (println row-sep)))))

; The positions on the board are indexed from 0 to 80 (inclusive)
(defn rows [board sz]
  (partition sz board))

(defn row-for [board index sz]
  (nth (rows board sz) (/ index 9)))

(defn column-for [board index sz]
  (let [col (mod index sz)]
    (map #(nth % col)
         (rows board sz))))

(defn subgrid-for [board i]
  (let [rows (rows board 9)
        sgcol (/ (mod i 9) 3)
        sgrow (/ (/ i 9) 3)
        grp-col (column-for (mapcat #(partition 3 %) rows) sgcol 3)
        grp (take 3 (drop (* 3 (int sgrow)) grp-col))]
    (flatten grp)))

(defn numbers-present-for [board i]
  (set
   (concat (row-for board i 9)
           (column-for board i 9)
           (subgrid-for board i))))

(defn possible-placements [board index]
  (clojure.set/difference #{1 2 3 4 5 6 7 8 9}
                  (numbers-present-for board index)))

(defn pos [pred coll] (for [v coll :when (pred v)] (.indexOf coll v)))
(defn solve [board]
  (if-let [[i & _]
            (and (some '#{-} board)
                 (pos  '#{-} board))]
    (flatten (map #(solve (assoc board i %))
                  (possible-placements board i)))
    board))

(-> b1 prep print-board)
(-> b1 solve
              prep
              print-board)
;(def b2 (assoc (vec (repeat 81 '-)) 0 1))
; Doesn't work if there are mutiple solutions

(-> (assoc b1 0 9) solve
              prep
              print-board)
; For wrong problems it gives out an empty board :D
