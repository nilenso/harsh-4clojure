; Write a function which reverses the interleave process into x number of subsequences.
; http://www.4clojure.com/problem/43

(def rev_interleave (fn [seq n] (vals (group-by #(mod % n) seq))))

(pprint (rev_interleave [1 2 3 4 5 6] 2))
(pprint (rev_interleave (range 9) 3))
