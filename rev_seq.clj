;Reverse Sequence http://www.4clojure.com/problem/23
(fn ! [x]
  (if (= (count x) 0)
    x
    (cons (last x) (! (butlast x)))
  )
)
