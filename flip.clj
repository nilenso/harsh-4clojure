; Write a higher-order function which flips the order of the arguments of an input function.

; http://www.4clojure.com/problem/46

(fn flip [f] (fn [& args] (apply f (reverse args))))
