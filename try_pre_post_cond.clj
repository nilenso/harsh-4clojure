(defmulti plus (fn [x y] (class x)))
(defmethod plus String [x y] (str x y))
(defmethod plus Long [x y] (+ x y))

(prn (plus 1 2))
(prn (plus "Hello " "kitty" ))
