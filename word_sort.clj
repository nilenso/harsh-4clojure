; Write a function that splits a sentence up into a sorted list of words. Capitalization should not affect sort order and punctuation should be ignored.

; http://www.4clojure.com/problem/70

(def word-sort (fn [sent]
                 (let [words (map (partial apply str) (filter #(Character/isLetter (first %)) (partition-by #(Character/isLetter %) sent)))]
                   (sort-by #(.toLowerCase %) words))))

; With threading
(defn word-sort [sent]
  (->> 
   sent
   (partition-by #(Character/isLetter %))
   (filter #(Character/isLetter (first %)))
   (map (partial apply str))
   (sort-by #(.toLowerCase %))))

(word-sort "Have a nice day.")
