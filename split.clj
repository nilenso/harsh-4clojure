; Write a function which will split a sequence into two parts.

; http://www.4clojure.com/problem/49

(fn split [n seq]
  [(take n seq) (drop n seq)])
